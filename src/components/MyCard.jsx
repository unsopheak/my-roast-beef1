import React, { Component } from 'react'
import {Card,Button, Col} from 'react-bootstrap'



export default class MyCard extends Component {
  render() {
    console.log("Foods Props:", this.props);
    return (
      <>
        {this.props.foods.map((item,index) =>
        (
          <Col xs="3" key={index}>
          <Card >
            <Card.Img variant="top" className="img" src={item.thumbnail} />
            <Card.Body>
              <Card.Title>{item.name}</Card.Title>
              <Card.Text>
                {item.price} $
                </Card.Text>
              <Button variant="info">{item.qty}</Button>{' '}
              <Button onClick={()=>this.props.onAdd(index)} variant="danger">Add</Button>{' '}
              <Button disabled={item.qty===0?true:false} onClick={()=>this.props.onDelete(index)}  variant="success">Delete</Button>
              <h4>Total: {item.total} $</h4>
            </Card.Body>
          </Card>
          </Col>
        ))}
      </>
    )
  }
}
