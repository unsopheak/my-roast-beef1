import React from 'react'
import {Table,Button} from 'react-bootstrap'
export default function MyTable({foods,onClear}) {
let temp=foods.filter(item=>{
return item.qty>0
})
    return (
        <>
        <div>
        <Button onClick={onClear} variant="primary" className="me-1">Clear All</Button>
       <Button variant="warning"><h6>{temp.length} count</h6></Button>
        </div>
        <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
          {temp.map((item,index)=>(
              <tr>
              <td>{index+1}</td>
              <td>{item.name}</td>
              <td>{item.qty}</td>
              <td>{item.price}</td>
              <td>{item.total}</td>
            </tr>
          ))}
        </tbody>
      </Table>
        </>
       
    )
}