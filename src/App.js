import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import MyCard from "./components/MyCard";
import { Container, Row } from "react-bootstrap";
import './App.css';
import MyTable from "./components/MyTable";
import Nav from "./components/Nav";

export default class App extends React.Component {

  constructor(props){
    super(props); 
    this.state={
      foods: [
        {
          id:1,
          name:"ROSEMARY BEEF",
          thumbnail:"/images/1.jpg",
          qty:0,
          price:20,
          total:0,
        },
        {
          id:2,
          name:"Roast Beef",
          thumbnail:"/images/2.jpg",
          qty:0,
          price:25,
          total:0,
        },
        {
          id:3,
          name:"Wine Braised Beef",
          thumbnail:"/images/3.jpg",
          qty:0,
          price:30,
          total:0,
        },
      {
        id:4,
          name:"Beef Tenderloin",
          thumbnail:"/images/4.jpg",
          qty:0,
          price:35,
          total:0,
      },
      ],
    }
  }
  onAdd =(index) => {
    console.log("index:",index);

    let temp=[...this.state.foods]
    temp[index].qty++
    temp[index].total=temp[index].qty * temp[index].price
    this.setState({
      foods: temp
    })   
  }

  onDelete =(index) => {
    console.log("index:",index);

    let temp=[...this.state.foods]
    temp[index].qty--
    temp[index].total=temp[index].qty * temp[index].price
    this.setState({
      foods: temp
    })   
  }
  onClear= ()=>{
    console.log("Clear");
    let temp=[...this.state.foods]
    temp.map(item=>{
      item.qty=0
      item.total=0
    })

    this.setState({
      foods: temp
    })
  }
  
  render() {
    return (
      <Container>
        <Row>
          <Nav></Nav>
        </Row>
        <Row>
          <MyCard foods={this.state.foods} onAdd={this.onAdd} onDelete={this.onDelete} />
        </Row>
        <Row>
          <MyTable  foods={this.state.foods} onClear={this.onClear}/>
        </Row>
      </Container>
    );
  }
}
